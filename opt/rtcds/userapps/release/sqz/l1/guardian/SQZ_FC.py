# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#
# $Id: SQZ_FC.py $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/sqz/l1/guardian/SQZ_FC.py $

import time
from guardian import GuardState, GuardStateDecorator, NodeManager, Node
import cdsutils

from optic import Optic

from guardian.state import (
    TimerManager,
)


from ezca_automon import (
    EzcaEpochAutomon,
    deco_autocommit,
    DecoShared,
)


import gpstime
import numpy as np
from noWaitServo import Servo

# SQZ config file (sqz/<ifo>/guardian/sqzconfig.py):
import sqzconfig

from scipy.optimize import curve_fit
from scipy import interpolate
import gpstime

##################################
####### ALIGNMENT STUFF #########
##################################

fc2 = Optic('FC2')


# this is to keep emacs python-mode happy from lint errors due to the namespace injection that Guardian does
if False:
    IFO = None
    ezca = None
    log = None
    notify = None

if sqzconfig.nosqz:
    nominal = 'IDLE'
else:
    nominal = 'LOCKED'


nodes = NodeManager(['SQZ_LO','SQZ_CLF', 'SQZ_OPO'])
node_LO = nodes['SQZ_LO']
node_CLF = nodes['SQZ_CLF']
node_OPO = nodes['SQZ_OPO']


# use a class with properties to access the RCG Epics-managed settings. If a module for setting is prefered like in the DRMI Guardian,
# then it is a drop-in replacement as both use attribute access
class EzcaUser(object):
    """
    has the necessary extra stuff to initialize classes using EzcaAccessor objects
    """
    pass

class SharedState(object):
    pass

class FCParams(EzcaUser):
    UNLOCK_TIMEOUT_S = 0.2
    RESONANCE_TIMEOUT_S = 1
    FAULT_TIMEOUT_S = 3

    # define locking gain and locked gain for green
    LOCKING_GAIN = 10 #was 10, BK 2025-02-18
    LOCKED_GAIN = 2 #10, 17
    
    lock_offset1 = 7.07 #0.92
    lock_offset2 = -7.23 #-2.54

    # for CLF_RLF transition
    OFS_IDX = 0

    def __init__(self):
        self.timer = TimerManager(logfunc = None)
        self.timer['FAULT_TIMEOUT'] = 0
        self.timer['UNLOCK_TIMEOUT'] = 0
        self.timer['REDLIGHT_TIMEOUT'] = 0
        self.timer['RESONANCE_TIMER'] = 0


    def GR_locked(self):
        factor = 1;
        refl = ezca['SQZ-FC_GRD_LOCK_GR_LTH']*factor < ezca['SQZ-FC_REFL_LF_OUTPUT'] < ezca['SQZ-FC_GRD_LOCK_GR_HTH']*factor
        transQPD = ezca['SQZ-FC_TRANS_A_SUM_OUT16'] > ezca['SQZ-FC_GRD_LOCK_GR_HTH']*factor
        #transQPD = ezca['SQZ-FC_TRANS_B_SUM_OUT16'] > ezca['SQZ-FC_GRD_LOCK_GR_HTH']*factor
        return transQPD

    def RLF_locked(self):
        RLF_resonating = ezca['SQZ-FC_WFS_A_I_SUM_NORM'] > 0.5
        
        return RLF_resonating
        
    def resonance_found(self):
        resonance_find = ezca['SQZ-FC_WFS_A_I_SUM_NORM'] > -0.5
        
        return resonance_find
        

class FCShared(SharedState):
    @DecoShared
    def fc(self):
        EzcaEpochAutomon.monkeypatch_subclass_instance(ezca)
        return FCParams()


shared = FCShared()


##################################################
# Functions
##################################################
def check_sus_output():
    output = False
    for sus in ['FC1','FC2']:
        for stage in ['M3','M2','M1']:
            output = output & bool(ezca['SUS-%s_%s_LOCK_L_OUTPUT'%(sus,stage)])
    return True
    return output

def check_dof_output():
    output = False
    for ii in range(4):
        dof = 'DOF%d'%(ii+1)
        output = output & bool(ezca['SQZ-FC_LSC_%s_OUTPUT'%dof])
    return True
    return output

def check_freq_output():
    output = False
    output = output & bool(ezca['SQZ-FC_LSC_FREQ_OUTPUT'])
    return True
    return output

def down():
    TRAMP = [0,0,0,0]
    wait = False

    # close beam diverter
    #ezca['SYS-MOTION_C_BDIV_E_OPEN'] = 0 # close diverter, Begum add this after deconflict with sqzmngr 2025-02-04

    # turn off suspension feedback
    ezca['SQZ-FC_ASC_GAIN'] = 0
    for sus in ['FC1','FC2']:
        for dof in ['P','Y']:
            #ezca.switch('SUS-%s_M1_LOCK_%s'%(sus,dof),'INPUT','OFF')
            None
            
    ezca['SQZ-FC_LSC_DOF1_TRAMP'] = 1
    time.sleep(0.1)
    ezca['SQZ-FC_LSC_DOF1_GAIN'] = 0
 
    # turn off LO and CLF loop
    node_LO.release()
    node_LO.set_request('IDLE')
    #node_CLF.release()
    #node_CLF.set_request('IDLE')

    
    if check_sus_output():
        # offload suspension
        for sus in ['FC1','FC2']:
            for stage in ['M1','M2','M3']:
                ezca['SUS-%s_%s_LOCK_L_TRAMP'%(sus,stage)] = 1
                ezca['SUS-%s_%s_LOCK_L_GAIN'%(sus,stage)] = 0
                
        
        wait = True

    for ii in range(4):
        dof = 'DOF%d'%(ii+1)
        
        # disable the LSC loop immediately and clear the history
        ezca['SQZ-FC_LSC_%s_TRAMP'%dof] = 0
    

    if wait:
        time.sleep(1)

        
    for ii in range(4):
        dof = 'DOF%d'%(ii+1)
        ezca['SQZ-FC_LSC_%s_RSET'%dof] = 2

    # Clear FC ASC history nicely
    tramp = 3
    ii = 0
    while ii<4:
        for axis in ['CAV','INJ']:
            for asc_dof in ['POS','ANG']:
                for dof in ['P','Y']:
                    if ii == 0:
                        ezca['SQZ-FC_ASC_{}_{}_{}_TRAMP'.format(axis,asc_dof,dof)] = tramp
                    elif ii == 1:
                        ezca['SQZ-FC_ASC_{}_{}_{}_GAIN'.format(axis,asc_dof,dof)] = 0
                    elif ii == 2:
                        ezca['SQZ-FC_ASC_{}_{}_{}_RSET'.format(axis,asc_dof,dof)] = 2
                    elif ii == 3:
                        ezca['SQZ-FC_ASC_{}_{}_{}_GAIN'.format(axis,asc_dof,dof)] = 1
        if ii == 0:
            time.sleep(0.2)
        elif ii == 1:
            time.sleep(tramp)
        elif ii == 2:
            time.sleep(0.2)
        ii+=1


    for sus in ['FC1','FC2']:
        for stage in ['M3','M2','M1']:
            ezca['SUS-%s_%s_LOCK_L_RSET'%(sus,stage)] = 2
            for dof in ['L2P','L2Y','P2L','P2Y','Y2L','Y2P']:
                ezca['SUS-%s_%s_DRIVEALIGN_%s_RSET'%(sus,stage,dof)] = 2


    for ii in range(4):
        dof = 'DOF%d'%(ii+1)
        
        # disable the LSC loop immediately and clear the history
        ezca['SQZ-FC_LSC_%s_GAIN'%dof] = 0

    # reset ISCINF
    ezca['SUS-FC1_M3_ISCINF_L_TRAMP'] = 0
    ezca['SUS-FC1_M3_ISCINF_L_GAIN'] = 0

    ezca['SUS-FC2_M3_ISCINF_L_TRAMP'] = 0
    ezca['SUS-FC2_M3_ISCINF_L_GAIN'] = 0


    time.sleep(0.2)
    
    for sus in ['FC1','FC2']:
        for stage in ['M3','M2','M1']:
            ezca['SUS-%s_%s_LOCK_L_TRAMP'%(sus,stage)] = 0.5            
            ezca['SUS-%s_%s_LOCK_L_GAIN'%(sus,stage)] = 1

            
    ezca.switch('SQZ-FC_LSC_DOF1','FM3','FM4','FM5','FM6','FM8','FM10','OFF','FM1','FM2','FM7','FM9','ON')
    ezca.switch('SQZ-FC_LSC_DOF3','FM3','FM4','FM5','FM6','FM7','FM8','FM10','OFF','FM1','FM2','FM9','ON')
    ezca.switch('SQZ-FC_LSC_DOF2','FM1','FM8','FM9','ON','FM2','FM3','FM4','FM5','FM6','FM7','FM10','OFF')

    
    #for axis in ['CAV','INJ']:
    #    for dof1 in ['POS','ANG']:
    #        for dof2 in ['P','Y']:
    #            dof = '%s_%s_%s'%(axis,dof1,dof2)
    #            # disable the LSC loop immediately and clear the history
    #            TRAMP = ezca['SQZ-FC_ASC_%s_TRAMP'%dof]
    #            ezca['SQZ-FC_ASC_%s_TRAMP'%dof] = 0
    #            ezca['SQZ-FC_ASC_%s_GAIN'%dof] = 0
    #    
    #            ezca['SQZ-FC_ASC_%s_RSET'%dof] = 2
    #            ezca['SQZ-FC_ASC_%s_TRAMP'%dof] = TRAMP
    

    # disable GR servo
    ezca['SQZ-FC_SERVO_IN1EN'] = 'Off'
    ezca['SQZ-FC_SERVO_IN2EN'] = 'Off'
    ezca['SQZ-FC_SERVO_COMBOOST'] = 0
    ezca['SQZ-FC_SERVO_SLOWBOOST'] = 0
    
    ezca['SQZ-LO_SERVO_IN2EN'] = 'Off'
    ezca['SQZ-LO_SERVO_COMBOOST'] = 0
    ezca['SQZ-LO_SERVO_SLOWBOOST'] = 0
    #ezca['SQZ-FC_SERVO_SLOWOFSEN'] = 'Off'

    # reset input matrix
    ezca['SQZ-FC_LSC_INMTRX_TRAMP'] = 0
    for ii in range(15):
        for jj in range(4):
            ezca['SQZ-FC_LSC_INMTRX_SETTING_%d_%d'%(jj+1,ii+1)] = 0
    ezca['SQZ-FC_LSC_INMTRX_LOAD_MATRIX'] = 1

    # reset fast switch
    ezca['SQZ-FC_LSC_FAST_SW_EN'] = 'OFF'

    # disable dither
    for ii in range(4):
        for dof in ['PIT','YAW']:
            ezca['SQZ-FC_ASC_ADS_%s%d_OSC_CLKGAIN'%(dof,ii+1)] = 0

    for dof1 in ['POS','ANG']:
        for dof2 in ['P','Y']:
            ezca.switch('SQZ-FC_ASC_INJ_%s_%s'%(dof1,dof2),'INPUT','OFF')


def ttfss_locked():
    return ezca['GRD-SQZ_TTFSS_STATE_S'] == 'LOCKED'

def isi_isolated(isi):
    return ezca['GRD-SEI_%s_STATE_S'%isi] == 'ISOLATED'
    
def sus_aligned(sus):
    return ezca['GRD-SUS_%s_STATE_S'%sus] == 'ALIGNED'

def rlf_vcxo_locked():
    if ezca['SQZ-RLF_VCXO_SERVO_OUT16'] > 1e5:
        ezca['SQZ-RLF_VCXO_SERVO_RSET'] = 2
        time.sleep(1)
        
    return ezca['SQZ-RLF_VCXO_SERVO_OUT16'] < 1e5


def clf_locked():    
    return ezca['GRD-SQZ_OPO_STATE_S'] == 'LOCKED_WITH_CLF' or ezca['GRD-SQZ_OPO_STATE_S'] == 'CLF_DITHER_LOCKED' or ezca['GRD-SQZ_CLF_STATE_S'] == 'CLF_REFL_LOCKED'
    # WJ temporarily changed this
    #return True

def is_fault():
    fault = False
    fault_str = ''
    # ISI check
    ISI_list = ['HAM7','HAM8']
    for isi in ISI_list:
        if not isi_isolated(isi):
            fault = True
            fault_str += '%s is not isolated, '%isi
    # SUS check
    SUS_list = ['FC1','FC2','ZM1','ZM2','ZM3']
    for sus in SUS_list:
        if not sus_aligned(sus):
            fault = True
            fault_str += '%s is not aligned, '%sus

    # RLF VCXO PLL check
    if not rlf_vcxo_locked():
        fault_str += 'RLF VCXO PLL loop is not locked.'
        fault = True
    
    return fault, fault_str
                
def notify_log(msg):
    notify(msg)
    log(msg)

def dif_lorentzian(ff, A, HWFM, ofs, DC):
    return A*ff/(HWFM**2+4*(ff-ofs)**2) + DC


##################################################
# Decorator
##################################################

class GR_lock_checker(GuardStateDecorator):
    def pre_exec(self):
        if not (shared.fc.GR_locked()):
            if shared.fc.timer['UNLOCK_TIMEOUT']:
                notify('Lost lock')
                shared.fc.OFS_IDX = 0 # BK 20240901, so that it starts checking from the - side after lockloss
                return 'DOWN'
            notify_log('GR might lost lock...')
            return
            
        else:
            shared.fc.timer['UNLOCK_TIMEOUT'] = shared.fc.UNLOCK_TIMEOUT_S


class RLF_lock_checker(GuardStateDecorator):
    def pre_exec(self):
        if not (shared.fc.RLF_locked()):
            if shared.fc.timer['UNLOCK_TIMEOUT']:
                notify('Lost lock')
                return 'DOWN'   #'DOWN' Begum changed this to INIT, to avoid STALL 250117, then changed back to DOWN because of SQZ_MANAGER stalling issues 250121
            notify_log('CLF might lost lock...')
            return
            
        else:
            shared.fc.timer['UNLOCK_TIMEOUT'] = shared.fc.UNLOCK_TIMEOUT_S
    
            
class fault_checker(GuardStateDecorator):
    def pre_exec(self):
        fault, fault_str = is_fault()
        if fault:
            notify(fault_str)            
            if shared.fc.timer['FAULT_TIMEOUT']:
                return 'FAULT'            
        else:
            shared.fc.timer['FAULT_TIMEOUT'] = shared.fc.FAULT_TIMEOUT_S
            
class resonance_checker(GuardStateDecorator):
    def pre_exec(self):
        if not (shared.fc.resonance_found()):
            if shared.fc.timer['RESONANCE_TIMEOUT']:
                notify('Couldnt find')
                return 'DOWN'
            notify_log('Maybe wrong resonance')
            return
            
        else:
            shared.fc.timer['RESONANCE_TIMER'] = shared.fc.RESONANCE_TIMEOUT_S

class ttfss_checker(GuardStateDecorator):
    def pre_exec(self):
        # check TTFSS guardian
        

        if not ttfss_locked():
            if shared.fc.timer['FAULT_TIMEOUT']:
                return 'DOWN'
            notify('ttfss not locked')
            
        else:
            shared.fc.timer['FAULT_TIMEOUT'] = shared.fc.FAULT_TIMEOUT_S
            
##################################################
# STATES
##################################################

#-------------------------------------------------------------------------------
class INIT(GuardState):
    index = 0
    request = True

    def run(self):
        return True

#-------------------------------------------------------------------------------
class IDLE(GuardState):
    index = 1
    request = True

    def main(self):
        # set output matrix for mass feedback
        ezca['SQZ-FC_LSC_INMTRX_TRAMP'] = 0
        ezca['SQZ-FC_LSC_INMTRX_SETTING_1_6'] = 1e6
        ezca['SQZ-FC_LSC_INMTRX_SETTING_3_5'] = 1
        ezca['SQZ-FC_LSC_INMTRX_SETTING_2_8'] = 25
        ezca['SQZ-FC_LSC_INMTRX_LOAD_MATRIX'] = 1

        time.sleep(0.1)
        ezca['SQZ-FC_LSC_INMTRX_TRAMP'] = 1

        # set servo
        ezca['SQZ-FC_LSC_DOF1_TRAMP'] = 1

        return True

#-------------------------------------------------------------------------------
class DOWN(GuardState):
    index = 2
    request = True
    goto = True

    def main(self):
        down()
        
    def run(self):
        return True

#-------------------------------------------------------------------------------
class FAULT(GuardState):
    index=3
    request = False

    def main(self):
        down()

    def run(self):
        fault, fault_str = is_fault()
        if fault:
            notify(fault_str)

        else:
            return True

#-------------------------------------------------------------------------------
class MISALIGN_FC2(GuardState):
    index = 7
    request = True
    
    def main(self):

        fc2.misalign('Y',1000,ezca)

        self.timer['done'] = 5 #0
        #self.counter = 0

    def run(self):

        if not self.timer['done']:
            return

        return True
#-------------------------------------------------------------------------------
class MANAGE_DEPENDENCIES(GuardState):
    index = 4
    request = False
    
    @fault_checker
    def main(self):
        node_LO.set_managed()
        node_LO.set_request('IDLE')
        #node_CLF.set_managed()
        #node_CLF.set_request('IDLE')

        if not rlf_vcxo_locked():
            ezca['SQZ-RLF_VCXO_SERVO_RSET'] = 2
        
    @fault_checker
    def run(self):
        return True

#-------------------------------------------------------------------------------
class NORMALIZE_WFS(GuardState):
    index = 5
    request = False
    
    def main(self):
        # for WFS normalization
        self.N = 0
        self.avgval = {'A':0,'B':0}
        self.avgtime = 1
        self.counter = 0

    def run(self):       

        if not True:#not clf_locked(): skip for test
                notify_log('CLF is not locked')
                return False
        else:
                 
            if self.counter == 0:
                # start normalize WFS signal
                print('counter = ', self.counter)
                self.timer['WFSavg'] = self.avgtime
                self.counter += 1
           

            elif self.counter == 1:
                 # average
                print('counter = ', self.counter)
                if not self.timer['WFSavg']:
                    self.N += 1
                    for WFS in ['A','B']:
                        self.avgval[WFS] += ezca['SQZ-FC_WFS_%s_I_SUM_OUT16'%WFS]

                else:
                    for WFS in ['A','B']:
                        for PHASE in ['I','Q']:
                            for dof in ['PIT','YAW','SUM']:
                                ezca['SQZ-FC_WFS_%s_%s_%s_POW_NORM'%(WFS,PHASE,dof)] = -self.avgval[WFS]/self.N
                
                    self.counter += 1
            elif self.counter == 2:
                    return True

#-------------------------------------------------------------------------------

class GR_LOCKING(GuardState):
    index = 6
    request = False

    
    def down_local(self):
        log('Lost lock, running down')
        ezca['SQZ-FC_SERVO_IN1EN'] = 'Off'
        ezca['SQZ-FC_SERVO_SLOWBOOST'] = 0
        ezca['SQZ-FC_SERVO_COMBOOST'] = 0
        ezca['SQZ-FC_SERVO_IN1GAIN'] = shared.fc.LOCKING_GAIN
        
    @ttfss_checker
    @fault_checker
    def main(self):
        self.LOCKCHECK_T = 0.1
        self.TIMEOUT_T = 20
        self.counter = 0
        self.timer['wait'] = 0
        

    @ttfss_checker
    @fault_checker
    def run(self):
        ezca['SQZ-FC_SERVO_SLOW_TRAMP'] = 1
#            ezca['SQZ-FC_SERVO_SLOW_OFFSET'] = e
        ezca['SQZ-FC_SERVO_SLOW_OFFSET'] = + 0
        ezca['SQZ-FC_SERVO_SLOWOUTOFS'] = + 0.44
        ezca['SQZ-FC_VCO_TUNEOFS'] = + 0
        
        # open flipper
        if self.counter == 0:
            ezca['SQZ-FC_FIBR_FLIPPER_CONTROL'] = 'On'
            self.counter += 1
            self.timer['done'] = 1  #FIXME: Nothing is waiting on this timer?

        # engage servo
        elif self.counter == 1:
            log('Engaging FC servo')
            ezca['SQZ-FC_SERVO_IN1GAIN'] = shared.fc.LOCKING_GAIN
            ezca['SQZ-FC_LSC_DOF1_GAIN'] = 0
            ezca['SQZ-FC_SERVO_IN1EN'] = 'On'
            self.timer['locked'] = self.LOCKCHECK_T
            self.timer['timeout'] = self.TIMEOUT_T
            self.counter += 1

        # wait until cavity locks, if longer than 20secs, toggle the servo
        elif self.counter == 2:
            if self.timer['timeout']:
                self.down_local()
                self.counter = 0
                notify_log('timeout... Toggle the servo.')
            
            if not shared.fc.GR_locked():
                self.timer['locked'] = self.LOCKCHECK_T
            
            else:
                if self.timer['locked']:
                    self.counter += 1

        # engage boosts
        elif self.counter == 3:
            log('Engage FC servo boosts')
            ezca['SQZ-FC_SERVO_SLOWBOOST'] = 1
            #ezca['SQZ-FC_SERVO_SLOWBOOST'] = 2 
            time.sleep(1)
            ezca['SQZ-FC_SERVO_COMBOOST'] = 1
            self.timer['lock_check'] = self.LOCKCHECK_T
            self.counter += 1

        # if still locked move on
        elif self.counter == 4:
            if not shared.fc.GR_locked():
                self.down_local()
                self.counter = 0
                
            if self.timer['lock_check']:                
                return True

#-------------------------------------------------------------------------------
class GR_MASS_FB(GuardState):
    index = 9
    request = False
    
    @ttfss_checker
    @fault_checker    
    @GR_lock_checker        
    def main(self):

        # make sure FC SERVO SLOW filter bank is set correctly
        ezca.switch('SQZ-FC_SERVO_SLOW','FM1','FM2','FM3','FM8','FM9','FM10','ON')

        # make sure FC paths are open
        ezca['SUS-FC1_M3_ISCINF_L_TRAMP'] = 0
        ezca['SUS-FC1_M3_ISCINF_L_GAIN'] = 1
        ezca['SUS-FC1_M3_ISCINF_L_TRAMP'] = 1        

        ezca['SUS-FC2_M3_ISCINF_L_TRAMP'] = 0
        ezca['SUS-FC2_M3_ISCINF_L_GAIN'] = 1
        ezca['SUS-FC2_M3_ISCINF_L_TRAMP'] = 1                
        self.counter = 0
        self.timer['wait'] = 0
        
        #send VCO to 0
        ezca['SQZ-FC_VCO_TUNEOFS'] = 0
        #ezca['SQZ-FC_VCO_TUNEOFS'] = -4.41  # July29 Adam, Begum
        
    @ttfss_checker
    @fault_checker
    @GR_lock_checker        
    def run(self):

        # LO node should be IDLE
        if not node_LO.state == 'IDLE':
            notify('LO is not IDLING. Return to DOWN')
            return 'DOWN'
        
        if not self.timer['wait']:
            return    

        # Turn on feedback to FC length            
        if self.counter == 0:
            log('Feeding back to FC length.')
            ezca['SQZ-FC_LSC_DOF1_GAIN'] = 0.1
            self.timer['wait'] = 1
            self.counter += 1

        # Turn off 0.3Hz highpass
        elif self.counter == 1:
            ezca.switch('SQZ-FC_LSC_DOF1','FM1','OFF')
            self.timer['wait'] = 3
            self.counter += 1

        elif self.counter ==2:
            length = (ezca['SQZ-FC_SERVO_IN1GAIN']-shared.fc.LOCKED_GAIN)+1
            for ii in range(1,length+2):
                if ezca['SQZ-FC_SERVO_IN1GAIN'] > shared.fc.LOCKED_GAIN: 
                    ezca['SQZ-FC_SERVO_IN1GAIN'] -= 1
                    self.timer['done'] = 0.1

            ezca['SQZ-FC_LSC_DOF1_TRAMP'] = 5
            ezca['SQZ-FC_LSC_DOF1_GAIN'] = 1
            self.counter += 1

        elif self.counter == 3:
            return True

#------------------------------------------------------------------------------- 
class GR_LOCKED(GuardState):
    index = 10
    request = True
    def main(self):
        self.counter = 0
        
    @ttfss_checker
    @fault_checker        
    @GR_lock_checker        
    def run(self):
        if self.counter == 0:
            return True




#-------------------------------------------------------------------------------
class PREP_FOR_IR(GuardState):
    index = 57
    request = True

    @ttfss_checker
    @fault_checker    
    @GR_lock_checker            
    def main(self):

        ezca['SQZ-FC_VCO_TUNEOFS'] = 0

        # initialize fast switch
        ezca['SQZ-FC_LSC_FAST_SW_EN'] = 0
        for ii in range(14):
            for jj in range(2):
                ezca['SQZ-FC_LSC_FAST_SW_INMTRX_%d_%d'%(jj+1,ii+1)] = 0
        #TODO: set up matrix file
        # GS_SL -> ERR1
        ezca['SQZ-FC_LSC_FAST_SW_INMTRX_1_6'] = 1e6
        # RLFCLF-WFSA_Q -> ERR2
        ezca['SQZ-FC_LSC_FAST_SW_INMTRX_2_8'] = -10.9*4
                
        
        # set input matrix to use FAST SW (initially set for green, see above)
        ezca['SQZ-FC_LSC_INMTRX_TRAMP'] = 0.5
        # GS_SL -> DOF1 = 0
        ezca['SQZ-FC_LSC_INMTRX_SETTING_1_6'] = 0
        # FAST_SW -> DOF1 = 1
        ezca['SQZ-FC_LSC_INMTRX_SETTING_1_15'] = 1 #this was 1
        ezca['SQZ-FC_LSC_INMTRX_LOAD_MATRIX'] = 1        
        time.sleep(0.5)

    def run(self):
         return True


#-------------------------------------------------------------------------------
class PRE_SCAN(GuardState):
    index = 58
    request = False

    @ttfss_checker
    @fault_checker    
    @GR_lock_checker            
    def main(self):

        self.sweeprange = 1 # cnts
        self.sweeptime = 10 # sec
        self.stepsize = 0.0025
        
        #----------------------------------------------
        self.sv1 = 7.57 #7.07 #1.6
        self.sv2 = self.sv1 - self.sweeprange #V?
        ezca['SQZ-FC_SERVO_SLOW_OFFSET'] = 6800
        
        if shared.fc.lock_offset1 > 6.0 and shared.fc.lock_offset1 < 8:
            self.sv1 = shared.fc.lock_offset1 + self.sweeprange/2
            self.sv2 = self.sv1 - self.sweeprange #V?

        # FIXME: Don't need this statement at all
        else: 
            pass

        self.sv3 = -7.73 #-7.23 #-3.2
        self.sv4 = self.sv3 + self.sweeprange #V?

        #FIXME: Typo: Should be offset2 (not offset1) in second inequality below
        if shared.fc.lock_offset2 < -6.2 and shared.fc.lock_offset2 > -8:
            self.sv3 = shared.fc.lock_offset2 - self.sweeprange/2
            self.sv4 = self.sv3 + self.sweeprange #V?
            
        else: 
            pass

        #----------------------------------------------
        # FIXME: Replace above with:

#        if shared.fc.lock_offset1 > 0.4 and shared.fc.lock_offset1 < 2:
#            self.sv1 = shared.fc.lock_offset1 + self.sweeprange/2
#        else:
#            self.sv1 = 1.6
#
#        self.sv2 = self.sv1 - self.sweeprange #V?
#
#        if shared.fc.lock_offset2 < -2 and shared.fc.lock_offset2 > -3.2:
#            self.sv3 = shared.fc.lock_offset2 - self.sweeprange/2
#        else:
#            self.sv3 = -3.2
#
#        self.sv4 = self.sv3 + self.sweeprange #V?

#        OR

#        self.sv = [1.6,-3.2]
#        if shared.fc.lock_offset1 > 0.4 and shared.fc.lock_offset1 < 2:
#            self.sv[0] = shared.fc.lock_offset1 + self.sweeprange/2
#        if shared.fc.lock_offset2 < -2 and shared.fc.lock_offset2 > -3.2:
#            self.sv[1] = shared.fc.lock_offset2 - self.sweeprange/2

        #----------------------------------------------

        #ezca['SQZ-FC_VCO_TUNEOFS'] = -2.5

        self.timer['done'] = 1

        self.counter = 0
        self.failcounter = 0
        self.flag = 0

        self.sign = -1

        
    @ttfss_checker
    @fault_checker
    @GR_lock_checker                
    def run(self):
  
        if not self.timer['done']:
            return

        if not clf_locked():
            notify_log('CLF is not locked')
            return

        if shared.fc.RLF_locked():
            return True

        if self.counter == 0:
            
            step_num = self.sweeprange/self.stepsize +1
            ezca['SQZ-FC_VCO_TUNEOFS'] = self.sv1
              
            for ii in range (int(step_num)):
                 if ezca['SQZ-FC_WFS_A_I_SUM_NORM'] >= -0.5:
                    # save this ezca['SQZ-FC_VCO_TUNEOFS'] value
                    notify('Found RLF resonance at %0.2f V'%(ezca['SQZ-FC_VCO_TUNEOFS']))
                    self.flag =1
                    time.sleep(1)
                    break
                    
#                 if ezca['SQZ-FC_WFS_A_I_SUM_NORM'] <= -1.2:
#                     #save this ezca['SQZ-FC_VCO_TUNEOFS'] value
#                     notify('Wrong side')
#                     self.flag =0
#                     break
                    
                 
                 if (ezca['SQZ-FC_VCO_TUNEOFS'] > self.sv2) and self.flag ==0:
                     ezca['SQZ-FC_VCO_TUNEOFS'] -= self.stepsize
                     
                     if ezca['SQZ-FC_WFS_A_I_SUM_NORM'] >= -0.5:
                        self.flag =1
                        # save this ezca['SQZ-FC_VCO_TUNEOFS'] value
                        shared.fc.green_res = ezca['SQZ-FC_VCO_TUNEOFS']
                        notify('Found RLF resonance at %0.2f V'%(ezca['SQZ-FC_VCO_TUNEOFS']))
                        time.sleep(1)
                        break
                     
                     time.sleep(0.025)
                     
                     if ezca['SQZ-FC_WFS_A_I_SUM_NORM'] >= -0.5:
                        # save this ezca['SQZ-FC_VCO_TUNEOFS'] value
                        shared.fc.green_res = ezca['SQZ-FC_VCO_TUNEOFS']
                        notify('Found RLF resonance at %0.2f V'%(ezca['SQZ-FC_VCO_TUNEOFS']))
                        self.flag =1
                        time.sleep(1)
                        break
                        
                     time.sleep(0.025)
                     
                     if ezca['SQZ-FC_WFS_A_I_SUM_NORM'] >= -0.5:
                        # save this ezca['SQZ-FC_VCO_TUNEOFS'] value
                        shared.fc.green_res = ezca['SQZ-FC_VCO_TUNEOFS']
                        notify('Found RLF resonance at %0.2f V'%(ezca['SQZ-FC_VCO_TUNEOFS']))
                        self.flag =1
                        time.sleep(1)
                        break                         
                 
                 elif ezca['SQZ-FC_VCO_TUNEOFS'] >=5 or ezca['SQZ-FC_VCO_TUNEOFS'] <=-5:
                     notify('FC VCO railed.')
                     
                     
            if self.flag ==1:
                #self.timer['swept'] = self.sweeptime + 0.5
                self.counter += 2
                self.subcounter = 0
                #@GR_lock_checker
                notify('Found RLF resonance at %0.2f V'%(ezca['SQZ-FC_VCO_TUNEOFS']))
                #self.counter += 2
                time.sleep(1)
                return True
                
            else: 
                
                if self.failcounter > 0:
                    self.failcounter +=1
                else:
                    pass
                
                if (int(self.failcounter/2))%2 == 0 and self.failcounter<8:
                    self.counter = 1
                    #self.failcounter +=1
                    notify('No RLF resonance on this side, check other')
                    time.sleep(1)
                else:
                
                    self.counter = 0
                    #self.failcounter +=1
                    notify('No RLF resonance on this side, check again')
                    time.sleep(1)
                    
                
   
                
        elif self.counter == 1:
        
            notify('Checking negative side')
            ezca['SQZ-FC_VCO_TUNEOFS'] = self.sv3 
            step_num = self.sweeprange/self.stepsize +1
              
            for ii in range (int(step_num)):
                 if ezca['SQZ-FC_WFS_A_I_SUM_NORM'] >= -0.5:
                    # save this ezca['SQZ-FC_VCO_TUNEOFS'] value
                    shared.fc.green_res = ezca['SQZ-FC_VCO_TUNEOFS']
                    notify('Found RLF resonance at %0.2f V'%(ezca['SQZ-FC_VCO_TUNEOFS']))
                    self.flag =1
                    time.sleep(1)
                    break
                    
                 
                 if (ezca['SQZ-FC_VCO_TUNEOFS'] < self.sv4) and self.flag ==0:
                     ezca['SQZ-FC_VCO_TUNEOFS'] += self.stepsize
                     
                     if ezca['SQZ-FC_WFS_A_I_SUM_NORM'] >= -0.5:
                        self.flag =1
                        # save this ezca['SQZ-FC_VCO_TUNEOFS'] value
                        shared.fc.green_res = ezca['SQZ-FC_VCO_TUNEOFS']
                        notify('Found RLF resonance at %0.2f V'%(ezca['SQZ-FC_VCO_TUNEOFS']))
                        time.sleep(1)
                        break
                     
                     time.sleep(0.025)
                     
                     if ezca['SQZ-FC_WFS_A_I_SUM_NORM'] >= -0.5:
                        # save this ezca['SQZ-FC_VCO_TUNEOFS'] value
                        shared.fc.green_res = ezca['SQZ-FC_VCO_TUNEOFS']
                        notify('Found RLF resonance at %0.2f V'%(ezca['SQZ-FC_VCO_TUNEOFS']))
                        self.flag =1
                        time.sleep(1)
                        break
                        
                     time.sleep(0.025)  
                     
                     if ezca['SQZ-FC_WFS_A_I_SUM_NORM'] >= -0.5:
                        # save this ezca['SQZ-FC_VCO_TUNEOFS'] value
                        shared.fc.green_res = ezca['SQZ-FC_VCO_TUNEOFS']
                        notify('Found RLF resonance at %0.2f V'%(ezca['SQZ-FC_VCO_TUNEOFS']))
                        self.flag =1
                        time.sleep(1)
                        break                       
                 
                 elif ezca['SQZ-FC_VCO_TUNEOFS'] >=5 or ezca['SQZ-FC_VCO_TUNEOFS'] <=-5:
                     notify('FC VCO railed.')
                         
                      
            if self.flag == 1:
                #self.timer['swept'] = self.sweeptime + 0.5
                self.counter += 1
                self.subcounter = 0
                #@GR_lock_checker
                notify('Found resonance.') 
                time.sleep(1)
                return True
                
            else: 
                self.failcounter +=1
                if (int(self.failcounter/2))%2 == 0 and self.failcounter<8:
                    self.counter = 1
                    #self.failcounter +=1
                    notify('No RLF resonance on this side, check again')
                    time.sleep(1)
                else:
                
                    self.counter = 0
                    #self.failcounter +=1
                    notify('No RLF resonance on this side, check other side')
                    time.sleep(1)

                                
                        
             
#-------------------------------------------------------------------------------
class LOCK_IR(GuardState):
    index = 59
    request = False

    @ttfss_checker
    @fault_checker    
    @GR_lock_checker            
    def main(self):
        self.counter = 0
        self.timer['done'] = 0

        self.sweeprange = 0.4
        if ezca['SQZ-FC_VCO_TUNEOFS'] > 0: 
            self.sign = 1
            self.startpos = ezca['SQZ-FC_VCO_TUNEOFS'] + (self.sweeprange/2)
        else:
            self.sign = 2
            self.startpos = ezca['SQZ-FC_VCO_TUNEOFS'] - (self.sweeprange/2)
            
        self.stepsize = 0.002

        self.fail_counter = 0
        self.LO_failcounter = 0
        
    @ttfss_checker
    @fault_checker
    @GR_lock_checker                
    def run(self):
  
        if not self.timer['done']:
            return False

        if not clf_locked():
            notify_log('CLF is not locked')
            return False


        if shared.fc.RLF_locked():
                return True

        if self.fail_counter > 3:
            notify_log('Keep failing to catch the IR resonance %d times. Please check the system.'%self.fail_counter)
         

        if self.counter == 0:
            # bring cavity to near the resonance candidate
            print('counter = ', self.counter)
            ezca['SQZ-FC_VCO_TUNEOFS'] = self.startpos 

            self.timer['done'] = 7
            self.counter += 1

        elif self.counter == 1:
            # reduce mass FB loop gain and request LO lock
            print('counter = ', self.counter)
            ezca['SQZ-FC_LSC_DOF1_GAIN'] = 0.1 
            time.sleep(1)

            ezca['SYS-MOTION_C_BDIV_E_OPEN'] = 1
            node_LO.set_request('SERVO_ENGAGED')
            self.timer['done'] = 1
            self.counter += 2
            
        elif self.counter == 3:
            # prepare the fine scan: DOF1 filter bank
            ezca['SQZ-FC_LSC_DOF1_GAIN'] = 0.5 #1
            time.sleep(0.5)
            ezca.switch('SQZ-FC_LSC_DOF1','FM3','ON')
            time.sleep(0.5)
            ezca.switch('SQZ-FC_LSC_DOF1','FM4','ON')
            time.sleep(0.5)
            self.timer['done'] = 1
            self.counter += 3
            
        elif self.counter == 6:
            # engage the Fast switch            
            ezca['SQZ-FC_LSC_FAST_SW_EN'] = 1
            time.sleep(0.3)            

            step_num = self.sweeprange/self.stepsize +1
            
            if self.sign ==1:
                        
                for ii in range (int(step_num)):
                     if not shared.fc.RLF_locked():
                         if ezca['SQZ-FC_VCO_TUNEOFS'] > self.startpos - self.sweeprange:
                             ezca['SQZ-FC_VCO_TUNEOFS'] -= self.stepsize
                             time.sleep(0.05)                    
                     
#                         elif ezca['SQZ-FC_VCO_TUNEOFS'] >= self.startpos + self.sweeprange:
#                             notify('overshot, try again')
#                             break
                         
                         elif ezca['SQZ-FC_VCO_TUNEOFS'] >=5 or ezca['SQZ-FC_VCO_TUNEOFS'] <=-5:
                             notify('FC VCO railed.')
                             break
                             
                     else: 
                         notify('FC IR locked!')
                         shared.fc.lock_offset1 = ezca['SQZ-FC_VCO_TUNEOFS']
                         self.counter += 1   
                         time.sleep(5)
                         #self.counter = 14
    #                     ezca['SQZ-FC_LSC_INMTRX_SETTING_1_8'] = -21.8 #Hz/cnts, -10.9
    #                     ezca['SQZ-FC_LSC_INMTRX_LOAD_MATRIX'] = 1
    #                    # FAST_SW -> DOF1 = 0
    #                     ezca['SQZ-FC_LSC_INMTRX_TRAMP'] = 5 #sec
    #                     ezca['SQZ-FC_LSC_INMTRX_SETTING_1_15'] = 0 #Hz/cnts
    #                     ezca['SQZ-FC_LSC_INMTRX_LOAD_MATRIX'] = 1
                         break
                         
            if self.sign ==2:
                        
                for ii in range (int(step_num)):
                     if not shared.fc.RLF_locked():
                         if ezca['SQZ-FC_VCO_TUNEOFS'] < self.startpos + self.sweeprange:
                             ezca['SQZ-FC_VCO_TUNEOFS'] += self.stepsize
                             time.sleep(0.05)                    
                     
#                         elif ezca['SQZ-FC_VCO_TUNEOFS'] >= self.startpos + self.sweeprange:
#                             notify('overshot, try again')
#                             break
                         
                         elif ezca['SQZ-FC_VCO_TUNEOFS'] >=5 or ezca['SQZ-FC_VCO_TUNEOFS'] <=-5:
                             notify('FC VCO railed.')
                             break
                             
                     else: 
                         notify('FC IR locked!')
                         shared.fc.lock_offset2 = ezca['SQZ-FC_VCO_TUNEOFS']
                         self.counter += 1 
                         time.sleep(5)
                         #self.counter = 14
    #                     ezca['SQZ-FC_LSC_INMTRX_SETTING_1_8'] = -21.8 #Hz/cnts, -10.9
    #                     ezca['SQZ-FC_LSC_INMTRX_LOAD_MATRIX'] = 1
    #                    # FAST_SW -> DOF1 = 0
    #                     ezca['SQZ-FC_LSC_INMTRX_TRAMP'] = 5 #sec
    #                     ezca['SQZ-FC_LSC_INMTRX_SETTING_1_15'] = 0 #Hz/cnts
    #                     ezca['SQZ-FC_LSC_INMTRX_LOAD_MATRIX'] = 1
                         break        
                 
                
            self.subcounter = 0
            

        elif self.counter == 7:
            if shared.fc.RLF_locked():
                self.timer['done'] = 0.5
                self.counter += 1
            else:
                if self.timer['swept']:
                    ezca['SQZ-FC_LSC_FAST_SW_EN'] = 0
                    # May not need to switch the filters back and LO off, may be able to just try again
                    # BK 20240901 
                    if self.subcounter == 0:
                        ezca.switch('SQZ-FC_LSC_DOF1','FM4','OFF')
                        time.sleep(0.5)
                        ezca.switch('SQZ-FC_LSC_DOF1','FM3','OFF')
                        time.sleep(0.5)                       
                        ezca['SQZ-FC_LSC_DOF1_GAIN'] = 0.05
                        self.timer['done'] = 2
                        self.subcounter += 1
                        
                    elif self.subcounter == 1:
                        node_LO.set_request('IDLE')
                        self.counter = 0
                        self.timer['done'] = 1                                                



        elif self.counter == 8:
            print('counter = ', self.counter)

            if shared.fc.RLF_locked():
                # Switch back off the Fast Switch mechanism
                # WFSA_Q -> DOF1
 
                ezca['SQZ-FC_LSC_INMTRX_SETTING_1_8'] = -10.9*2 #Hz/cnts, -10.9
                ezca['SQZ-FC_LSC_INMTRX_LOAD_MATRIX'] = 1
                # FAST_SW -> DOF1 = 0
                ezca['SQZ-FC_LSC_INMTRX_TRAMP'] = 5 #sec
                ezca['SQZ-FC_LSC_INMTRX_SETTING_1_15'] = 0 #Hz/cnts
                ezca['SQZ-FC_LSC_INMTRX_LOAD_MATRIX'] = 1
                
                #ezca['SQZ-FC_LSC_DOF1_GAIN'] = 0.25 #1

                self.timer['done'] = 1
                self.counter += 1

            else:
                return 'DOWN'
    
        # Maybe this final check is excessive. BK 20240901
        elif self.counter == 9:
            print('counter = ', self.counter)

            if shared.fc.RLF_locked():
                return True

            else:
                return 'DOWN'


#-------------------------------------------------------------------------------
class CLFRLF_MASS_FB_1(GuardState):
    index = 70
    request = True

    @ttfss_checker
    @fault_checker    
    @GR_lock_checker            
    def main(self):
        self.counter = 0
        self.timer['done'] = 0

        self.sweeptime = 40 # sec
        self.sweeprange = 1800 # cnts
        ezca['SQZ-FC_VCO_TUNEOFS'] = -4.41  # July29 Adam, Begum, to offload from FC CMB

        self.fail_counter = 0

        # for WFS normalization
        self.N = 0
        self.avgval = {'A':0,'B':0}
        self.avgtime = 1

        # initialize fast switch
        ezca['SQZ-FC_LSC_FAST_SW_EN'] = 0
        for ii in range(14):
            for jj in range(2):
                ezca['SQZ-FC_LSC_FAST_SW_INMTRX_%d_%d'%(jj+1,ii+1)] = 0
        #TODO: set up matrix file
        # GS_SL -> ERR1
        ezca['SQZ-FC_LSC_FAST_SW_INMTRX_1_6'] = 1e6
        # RLFCLF-WFSA_Q -> ERR2
        ezca['SQZ-FC_LSC_FAST_SW_INMTRX_2_8'] = -10.9*4
        ezca['SQZ-FC_LSC_DOF1_TRAMP'] = 1
        
        
        # set input matrix to use FAST SW (initially set for green, see above)
        ezca['SQZ-FC_LSC_INMTRX_TRAMP'] = 5
        # GS_SL -> DOF1 = 0
        ezca['SQZ-FC_LSC_INMTRX_SETTING_1_6'] = 0
        # FAST_SW -> DOF1 = 1
        ezca['SQZ-FC_LSC_INMTRX_SETTING_1_15'] = 1
        ezca['SQZ-FC_LSC_INMTRX_LOAD_MATRIX'] = 1        
        time.sleep(0.5)

        self.LO_failcounter = 0
        
    @ttfss_checker
    @fault_checker
    @GR_lock_checker                
    def run(self):
  
        if not self.timer['done']:
            return False

        if not clf_locked():
            notify_log('CLF is not locked')
            return False

        # waiting LO guardian
        if not node_LO.completed:
            notify('Waiting LO guardian')
            if node_LO.STALLED:
                self.LO_failcounter += 1
                if self.LO_failcounter > 4:
                    notify('LO might have a problem.')
                    return
                node_LO.revive()
            return

        if self.counter == 0:
            # turn LO and CLF off
            #node_LO.set_request('IDLE')
            #node_CLF.set_request('IDLE')            
            self.counter += 1

        elif self.counter == 1:
            # bring the FC off-res
            ezca['SQZ-FC_SERVO_SLOW_TRAMP'] = 5
            ezca['SQZ-FC_SERVO_SLOW_OFFSET'] = 0
            self.timer['done'] = 5
            self.counter += 1

        elif self.counter == 2:
            # start normalize WFS signal
            self.timer['WFSavg'] = self.avgtime
            self.counter += 1

        elif self.counter == 3:
            # average
            if not self.timer['WFSavg']:
                self.N += 1
                for WFS in ['A','B']:
                    self.avgval[WFS] += ezca['SQZ-FC_WFS_%s_I_SUM_OUT16'%WFS]

            else:
                for WFS in ['A','B']:
                    for PHASE in ['I','Q']:
                        for dof in ['PIT','YAW','SUM']:
                            ezca['SQZ-FC_WFS_%s_%s_%s_POW_NORM'%(WFS,PHASE,dof)] = -self.avgval[WFS]/self.N
                
                self.counter += 1

        elif self.counter == 4:
            # bring cavity to near the resonance candidate
            ezca['SQZ-FC_SERVO_SLOW_TRAMP'] = 5
            ezca['SQZ-FC_SERVO_SLOW_OFFSET'] = ezca['SQZ-FC_GRD_RES_OFFSET%d'%(shared.fc.OFS_IDX+1)] - self.sweeprange/2.
            self.timer['done'] = 7
            self.counter += 1

        elif self.counter == 5:
            return True

#-------------------------------------------------------------------------------
class CLFRLF_MASS_FB_2(GuardState):
    index = 80
    request = False

    @ttfss_checker
    @fault_checker    
    @GR_lock_checker            
    def main(self):

        self.counter = 1    #Skip the first step as it was done in the last state
        self.fail_counter = 0
        self.timer['done'] = 0
        self.LO_failcounter = 0
        self.sweep_counter = 0

        self.sweeptime = 40 # 15 sec
        self.sweeprange = 1800 #800 # cnts

    @ttfss_checker
    @fault_checker
    @GR_lock_checker                
    def run(self):

        if not self.timer['done']:
            return False

        # waiting LO guardian
        if not node_LO.completed:
            notify('Waiting LO guardian')
            if node_LO.STALLED:
                self.LO_failcounter += 1
                if self.LO_failcounter > 4:
                    notify('LO might have a problem.')
                    return
                node_LO.revive()
            return

        if self.counter == 0:
            # bring cavity to near the resonance candidate
            log('Bring cavity near resonance point')
            ezca['SQZ-FC_SERVO_SLOW_TRAMP'] = 5
            ezca['SQZ-FC_SERVO_SLOW_OFFSET'] = ezca['SQZ-FC_GRD_RES_OFFSET%d'%(shared.fc.OFS_IDX+1)] - self.sweeprange/2.
            time.sleep(5)
            self.timer['done'] = 7
            self.counter += 1

        elif self.counter == 1:
            # reduce mass FB loop gain
            log('Reducing FC LSC gain to 0.05')
            ezca['SQZ-FC_LSC_DOF1_GAIN'] = 0.03 #0.03 B.K. 
            time.sleep(1)
            self.timer['done'] = 1
            self.counter += 1

        elif self.counter == 2:
            # lock LO loop
            log('Locking LO')
            node_LO.set_request('SERVO_ENGAGED')
            time.sleep(5)
            # boost ttfss gain, BK did this 25-01-24 alog xxxxx
            ezca['SQZ-FIBR_SERVO_COMGAIN'] = 17
            ezca['SQZ-FIBR_SERVO_FASTGAIN'] = 16
            time.sleep(3)
            
            self.timer['done'] = 1
            self.counter += 1

        elif self.counter == 3:
            # prepare the fine scan
            log('Setting FC LSC gain back to 1')
            ezca['SQZ-FC_LSC_DOF1_GAIN'] = 1
            time.sleep(1)
            self.timer['done'] = 2
            self.counter += 1

        elif self.counter == 4:
            # MASS FB HBW
            log('Mass FB high bandwidth')
            ezca.switch('SQZ-FC_LSC_DOF1','FM3','ON')
            time.sleep(0.5)
            self.timer['done'] = 1
            self.counter += 1

        elif self.counter == 5:
            # BST and resG
            log('Engaging Boost and ResG')
            ezca.switch('SQZ-FC_LSC_DOF1','FM4','ON')
            time.sleep(0.5)
            self.timer['done'] = 1
            self.counter += 1

        elif self.counter == 6:
            # engage the Fast switch  
            log('Engaging Fast Switch')          
            ezca['SQZ-FC_LSC_FAST_SW_EN'] = 1
            time.sleep(0.3)            
            # start fine sweep
            log('Sweeping the FC green offset, looking for IR')
            ezca['SQZ-FC_SERVO_SLOW_TRAMP'] = self.sweeptime #*2
            ezca['SQZ-FC_SERVO_SLOW_OFFSET'] = ezca['SQZ-FC_GRD_RES_OFFSET%d'%(shared.fc.OFS_IDX+1)] + self.sweeprange/2.
            self.timer['swept'] = self.sweeptime + 0.5
            self.counter += 1
            self.subcounter = 0


        elif self.counter == 7:
            if shared.fc.RLF_locked():
                log('RLF locked')
                self.timer['done'] = 2
                self.counter += 1
            elif self.timer['swept']:
                log('Sweep finished, failed to switch to IR, disengage and try again')
                self.sweep_counter += 1 # BK 20240901
                ezca['SQZ-FC_LSC_FAST_SW_EN'] = 0
                
                # Condition, if sweep_counter>=3, unlock green and relock # BK 20240901
                if self.sweep_counter>=3: # BK 20240901
                    log('Failed to catch lock >= 3 times, unlocking green.') # BK 20240901
                    return 'DOWN'
                else: # BK 20240901
                    if self.subcounter == 0:
                        log('Turning off Boost and ResG')
                        ezca.switch('SQZ-FC_LSC_DOF1','FM4','OFF')
                        self.timer['done'] = 1
                        self.subcounter += 1
                        
                    elif self.subcounter == 1:
                        log('Turning off High Bandwidth')
                        ezca.switch('SQZ-FC_LSC_DOF1','FM3','OFF')
                        self.timer['done'] = 1
                        self.subcounter += 1
                        
                    elif self.subcounter == 2:
                        log('Set gain back')               
                        ezca['SQZ-FC_LSC_DOF1_GAIN'] = 0.03
                        self.timer['done'] = 2
                        self.subcounter += 1
                        
                    elif self.subcounter == 3:
                        log('Send LO back to IDLE')
                        node_LO.set_request('IDLE')
                        #node_CLF.set_request('IDLE')                        
                        shared.fc.OFS_IDX = not shared.fc.OFS_IDX
                        self.counter = 0
                        self.timer['done'] = 1  

        elif self.counter == 8:
            if shared.fc.RLF_locked():
                # Switch back off the Fast Switch mechanism
                # WFSA_Q -> DOF1
                ezca['SQZ-FC_LSC_INMTRX_SETTING_1_8'] = -10.9 #Hz/cnts
                # FAST_SW -> DOF1 = 0
                ezca['SQZ-FC_LSC_INMTRX_SETTING_1_15'] = 0 #Hz/cnts
                ezca['SQZ-FC_LSC_INMTRX_LOAD_MATRIX'] = 1
                self.timer['done'] = 1
                self.counter += 1
            else:
                return 'DOWN'

        elif self.counter == 9:
            if shared.fc.RLF_locked():
                # reduce ttfss gain, BK did this 25-01-24 alog xxxxx
                ezca['SQZ-FIBR_SERVO_COMGAIN'] = ezca['SQZ-TTFSS_GRD_COMGAIN_LOCK']
                ezca['SQZ-FIBR_SERVO_FASTGAIN'] = 20
                return True
            else:
                return 'DOWN'



#-------------------------------------------------------------------------------
# WJ adds this class
class SINGLE_CLFRLF_MASS_FB(GuardState):
    index = 61
    request = False

    @ttfss_checker
    @fault_checker    
    @GR_lock_checker            
    def main(self):
        self.counter = 0
        self.timer['done'] = 0

        self.sweeprange = 800 # cnts
        self.sweeptime = 15 # sec

        self.fail_counter = 0

        # for WFS normalization
        self.N = 0
        self.avgval = {'A':0,'B':0}
        self.avgtime = 1

        # initialize fast switch
        ezca['SQZ-FC_LSC_FAST_SW_EN'] = 0
        for ii in range(14):
            for jj in range(2):
                ezca['SQZ-FC_LSC_FAST_SW_INMTRX_%d_%d'%(jj+1,ii+1)] = 0

        ezca['SQZ-FC_LSC_FAST_SW_INMTRX_1_6'] = 1e6
        ezca['SQZ-FC_LSC_FAST_SW_INMTRX_2_8'] = -10.9*4
        ezca['SQZ-FC_LSC_DOF1_TRAMP'] = 1
        
        
        # set input matrix
        ezca['SQZ-FC_LSC_INMTRX_TRAMP'] = 0.5
        ezca['SQZ-FC_LSC_INMTRX_SETTING_1_6'] = 0
        ezca['SQZ-FC_LSC_INMTRX_SETTING_1_15'] = 1
        ezca['SQZ-FC_LSC_INMTRX_LOAD_MATRIX'] = 1        
        time.sleep(0.5)

        self.LO_failcounter = 0

                
    @ttfss_checker
    @fault_checker
    @GR_lock_checker                
    def run(self):        
        if not self.timer['done']:
            return False

        #if not clf_locked():
        #    notify_log('CLF is not locked')
        #    return False


        if self.fail_counter > 3:
            notify_log('Keep failing to catch the IR resonance %d times. Please check the system.'%self.fail_counter)

        # WJ commented it out to test 
        # waiting LO guardian
        #if not node_LO.completed:
        #    notify('Waiting LO guardian')
        #    if node_LO.STALLED:
        #        self.LO_failcounter += 1
        #        if self.LO_failcounter > 4:
        #            notify('LO might have a problem.')
        #            return
        #        node_LO.revive()
        #    return

        # waiting CLF guardian
        #if not node_CLF.completed:
        #    notify('Waiting CLF guardian')
        #    if node_CLF.STALLED:
        #        self.CLF_failcounter += 1
        #        if self.CLF_failcounter > 4:
        #            notify('LO might have a problem.')
        #            return
        #        node_CLF.revive()
        #    return
        

        if self.counter == 0:
            # turn LO and CLF off
            #node_LO.set_request('IDLE')
            #node_CLF.set_request('IDLE')           
            ezca['SYS-MOTION_C_BDIV_E_OPEN'] = 1 # open diverter
            time.sleep(2.0)
            self.counter += 1

        elif self.counter == 1:
            # bring the FC off-res
            ezca['SQZ-FC_SERVO_SLOW_TRAMP'] = 5
            ezca['SQZ-FC_SERVO_SLOW_OFFSET'] = 0
            self.timer['done'] = 5
            #self.counter += 1
            self.counter += 4
            
        elif self.counter == 2:
            # start normalize WFS signal
            self.timer['WFSavg'] = self.avgtime
            self.counter += 1

        elif self.counter == 3:
            # average
            if not self.timer['WFSavg']:
                self.N += 1
                for WFS in ['A','B']:
                    self.avgval[WFS] += ezca['SQZ-FC_WFS_%s_I_SUM_OUT16'%WFS]

            else:
                for WFS in ['A','B']:
                    for PHASE in ['I','Q']:
                        for dof in ['PIT','YAW','SUM']:
                            ezca['SQZ-FC_WFS_%s_%s_%s_POW_NORM'%(WFS,PHASE,dof)] = -self.avgval[WFS]/self.N
                
                self.counter += 1

        elif self.counter == 4:
            # bring cavity to near the resonance candidate
            ezca['SQZ-FC_SERVO_SLOW_TRAMP'] = 5
            ezca['SQZ-FC_SERVO_SLOW_OFFSET'] = ezca['SQZ-FC_GRD_RES_OFFSET%d'%(shared.fc.OFS_IDX+1)] - self.sweeprange/2.
            self.timer['done'] = 7
            self.counter += 1

        elif self.counter == 5:
            # reduce mass FB loop gain
            ezca['SQZ-FC_LSC_DOF1_GAIN'] = 0.03
            self.timer['done'] = 1
            self.counter += 1

        elif self.counter == 6:
            print('self.counter = ', self.counter)
            # lock LO loop

            # WJ changed these lines
            #if not ezca['GRD-SQZ_OPO_STATE_S'] == 'SINGLE_CLF_ALIGNED':
            #    node_CLF.set_request('SINGLE_CLF_ALIGNED')

            node_LO.set_request('SERVO_ENGAGED')
            self.timer['done'] = 1
            self.counter += 1
            
        elif self.counter == 7:
            print('self.counter = ', self.counter)
            # lock CLF loop
            #node_CLF.set_request('LOCKED')
            self.timer['done'] = 0
            self.counter += 1

            
        elif self.counter == 8:
            print('self.counter = ', self.counter)
            # prepare the fine scan
            ezca['SQZ-FC_LSC_DOF1_GAIN'] = 1
            self.timer['done'] = 2
            self.counter += 1

        elif self.counter == 9:
            print('self.counter = ', self.counter)
            # MASS FB HBW
            ezca.switch('SQZ-FC_LSC_DOF1','FM3','ON')
            self.timer['done'] = 1
            self.counter += 1
            
        elif self.counter == 10:
            print('self.counter = ', self.counter)
            # BST and resG
            ezca.switch('SQZ-FC_LSC_DOF1','FM4','ON')
            self.timer['done'] = 1
            self.counter += 1
            
        elif self.counter == 11:
            print('self.counter = ', self.counter)
            # engage the Fast switch            
            ezca['SQZ-FC_LSC_FAST_SW_EN'] = 1
            time.sleep(0.3)            
            # start fine sweep
            ezca['SQZ-FC_SERVO_SLOW_TRAMP'] = self.sweeptime
            ezca['SQZ-FC_SERVO_SLOW_OFFSET'] = ezca['SQZ-FC_GRD_RES_OFFSET%d'%(shared.fc.OFS_IDX+1)] + self.sweeprange/2.
            self.timer['swept'] = self.sweeptime + 0.5
            self.counter += 1
            self.subcounter = 0
            

        elif self.counter == 12:
            print('self.counter = ', self.counter)

            if shared.fc.RLF_locked():
                self.timer['done'] = 0.5
                self.counter += 1
            else:
                if self.timer['swept']:
                    ezca['SQZ-FC_LSC_FAST_SW_EN'] = 0
                    if self.subcounter == 0:
                        ezca.switch('SQZ-FC_LSC_DOF1','FM4','OFF')
                        self.timer['done'] = 1
                        self.subcounter += 1
                        
                    elif self.subcounter == 1:
                        ezca.switch('SQZ-FC_LSC_DOF1','FM3','OFF')
                        self.timer['done'] = 1
                        self.subcounter += 1
                        
                    elif self.subcounter == 2:                        
                        ezca['SQZ-FC_LSC_DOF1_GAIN'] = 0.03
                        self.timer['done'] = 2
                        self.subcounter += 1
                        
                    elif self.subcounter == 3:
                        node_LO.set_request('IDLE')
                        #node_CLF.set_request('IDLE')                        
                        shared.fc.OFS_IDX = not shared.fc.OFS_IDX
                        self.counter = 4
                        self.timer['done'] = 1                                                

        elif self.counter == 13:
            print('self.counter = ', self.counter)

            if shared.fc.RLF_locked():
                self.timer['done'] = 2
                self.counter += 1
            else:
                self.timer['done'] = 0.1
                shared.fc.OFS_IDX = not shared.fc.OFS_IDX
                self.counter = 4

        elif self.counter == 14:
            if shared.fc.RLF_locked():
                ezca['SQZ-FC_LSC_INMTRX_SETTING_1_8'] = -10.9 #Hz/cnts
                ezca['SQZ-FC_LSC_INMTRX_SETTING_1_15'] = 0 #Hz/cnts
                ezca['SQZ-FC_LSC_INMTRX_LOAD_MATRIX'] = 1
                self.timer['done'] = 1
                self.counter += 1

            else:
                return 'DOWN'

        elif self.counter == 15:
            if shared.fc.RLF_locked():
                return True

            else:
                return 'DOWN'

#-------------------------------------------------------------------------------
class RLF_LOCKED(GuardState):
    index = 100
    request = True
    
    def main(self):
        ezca['SQZ-FC_LSC_INMTRX_SETTING_1_8'] = -21.8 #Hz/cnts, -10.9
        ezca['SQZ-FC_LSC_INMTRX_LOAD_MATRIX'] = 1
        # FAST_SW -> DOF1 = 0
        ezca['SQZ-FC_LSC_INMTRX_TRAMP'] = 5 #sec
        ezca['SQZ-FC_LSC_INMTRX_SETTING_1_15'] = 0 #Hz/cnts
        ezca['SQZ-FC_LSC_INMTRX_LOAD_MATRIX'] = 1
    
    @ttfss_checker
    @fault_checker
    @RLF_lock_checker
    def run(self):
        return True

#-------------------------------------------------------------------------------
#WJ added this function
class SINGLE_RLF_LOCKED(GuardState):
    index = 102
    request = False
    
    @ttfss_checker
    @fault_checker
    @RLF_lock_checker
    def run(self):
        return True

#-------------------------------------------------------------------------------
class IRFREQ_LOCKING(GuardState):
    index = 45
    request = False

    def down_local(self):
        ezca['SQZ-LO_SERVO_IN2EN'] = 'Off'
        ezca['SQZ-LO_SERVO_SLOWBOOST'] = 0
        ezca['SQZ-LO_SERVO_COMBOOST'] = 0
        
    @ttfss_checker
    @fault_checker
    def main(self):
        self.LOCKCHECK_T = 0.1
        self.TIMEOUT_T = 20
        self.counter = 0
        self.timer['wait'] = 0

    @ttfss_checker
    @fault_checker
    def run(self):
        if self.counter == 0:
            # open flipper
            ezca['SQZ-FC_FIBR_FLIPPER_CONTROL'] = 'On'
            self.counter += 1
            self.timer['done'] = 1
        
        elif self.counter == 1:
            # engage servo
            ezca['SQZ-LO_SERVO_IN2EN'] = 'On'
            self.timer['locked'] = self.LOCKCHECK_T
            self.timer['timeout'] = self.TIMEOUT_T
            self.counter += 1
            
        elif self.counter == 2:
            if self.timer['timeout']:
                self.down_local()
                self.counter = 0
                notify_log('timeout... Toggle the servo.')
            
            if not shared.fc.GR_locked():
                self.timer['locked'] = self.LOCKCHECK_T
            
            else:
                if self.timer['locked']:
                    self.counter += 1
        elif self.counter == 3:
            ezca['SQZ-LO_SERVO_SLOWBOOST'] = 1
            time.sleep(1)
            ezca['SQZ-LO_SERVO_COMBOOST'] = 1
            self.timer['lock_check'] = self.LOCKCHECK_T
            self.counter += 1

        elif self.counter == 4:
            if not shared.fc.GR_locked():
                self.down_local()
                self.counter = 0
                
            if self.timer['lock_check']:                
                return True

#-------------------------------------------------------------------------------
class IRFREQ_MASS_FB(GuardState):
    index = 46
    request = False
    
    @ttfss_checker
    @fault_checker    
    @GR_lock_checker        
    def main(self):
        ezca['SUS-FC1_M3_ISCINF_L_TRAMP'] = 0
        ezca['SUS-FC1_M3_ISCINF_L_GAIN'] = 1
        ezca['SUS-FC1_M3_ISCINF_L_TRAMP'] = 1        

        ezca['SUS-FC2_M3_ISCINF_L_TRAMP'] = 0
        ezca['SUS-FC2_M3_ISCINF_L_GAIN'] = 1
        ezca['SUS-FC2_M3_ISCINF_L_TRAMP'] = 1                
        self.counter = 0
        self.timer['wait'] = 0
        
    @ttfss_checker
    @fault_checker
    @GR_lock_checker        
    def run(self):
        if not self.timer['wait']:
            return False    
            
        if self.counter == 0:
            ezca['SQZ-FC_LSC_DOF3_GAIN'] = 1
            self.timer['wait'] = 1
            self.counter += 1

        elif self.counter == 1:
            ezca.switch('SQZ-FC_LSC_DOF3','FM1','OFF')
            self.timer['wait'] = 3
            self.counter += 1

        elif self.counter == 2:
            return True

#-------------------------------------------------------------------------------
class IRFREQ_LOCKED(GuardState):
    index = 50
    request = True
    def main(self):
        self.counter = 0
        
    @ttfss_checker
    @fault_checker        
    @GR_lock_checker        
    def run(self):
        if self.counter == 0:
            return True

#------------------------------------------------------------------------------- 
class IRFREQ_RLF_LOCKING(GuardState):
    index = 51
    request = False
    
    @fault_checker    
    @ttfss_checker    
    @GR_lock_checker            
    def main(self):
        self.counter = 0
        self.timer['done'] = 0

        self.sweeprange = 60 # cnts
        self.sweeptime = 10 # sec

        self.fail_counter = 0
        self.sign = [-1,1]


        # for WFS normalization
        self.N = 0
        self.avgval = {'A':0,'B':0}
        self.avgtime = 1

        self.even = False
        ezca['SQZ-FC_LSC_FREQ_TRAMP'] = 0
        time.sleep(0.3)
        ezca['SQZ-FC_LSC_FREQ_OFFSET'] = 0
        time.sleep(1)        
        ezca['SQZ-FC_SERVO_IN2EN'] = 'On'
        
    @fault_checker        
    @ttfss_checker
    @GR_lock_checker                
    def run(self):        
        if not self.timer['done']:
            return False

        if not clf_locked():
            notify_log('CLF is not locked')
            return False

        if self.fail_counter > 3:
            notify_log('Keep failing to catch the IR resonance %d times. Please check the system.'%self.fail_counter)

        if self.counter == 0:
            # start normalize WFS signal
            self.timer['WFSavg'] = self.avgtime
            self.counter += 1

        elif self.counter == 1:
            # average
            if not self.timer['WFSavg']:
                self.N += 1
                for WFS in ['A','B']:
                    self.avgval[WFS] += ezca['SQZ-FC_WFS_%s_I_SUM_OUT16'%WFS]

            else:
                for WFS in ['A','B']:
                    for PHASE in ['I','Q']:
                        for dof in ['PIT','YAW','SUM']:
                            ezca['SQZ-FC_WFS_%s_%s_%s_POW_NORM'%(WFS,PHASE,dof)] = -self.avgval[WFS]/self.N
                
                self.counter += 1

        elif self.counter == 2:
            # bring cavity to near the resonance candidate
            ezca['SQZ-FC_LSC_FREQ_TRAMP'] = 1
            ezca['SQZ-FC_LSC_FREQ_OFFSET'] = ezca['SQZ-FC_GRD_RES_LO_OFFSET%d'%(shared.fc.OFS_IDX+1)] - self.sweeprange/2.
            self.timer['done'] = 1.5
            self.counter += 1

        elif self.counter == 3:            
            # sweep around the resonance
            ezca['SQZ-FC_LSC_DOF2_TRAMP'] = 0
            ezca['SQZ-FC_LSC_DOF2_GAIN'] = 1
            time.sleep(1)
            
            ezca['SQZ-FC_LSC_FREQ_TRAMP'] = self.sweeptime
            ezca['SQZ-FC_LSC_FREQ_OFFSET'] = ezca['SQZ-FC_GRD_RES_LO_OFFSET%d'%(shared.fc.OFS_IDX+1)] + self.sweeprange/2.
            self.timer['swept'] = self.sweeptime + 0.5
            self.counter += 1

        elif self.counter == 4:
            if shared.fc.RLF_locked():
                ezca.switch('SQZ-FC_LSC_DOF2','FM2','ON')
                self.timer['done'] = 8
                self.counter += 1
            else:
                if self.timer['swept']:
                    ezca['SQZ-FC_LSC_DOF2_GAIN'] = 0
                    time.sleep(1)
                    ezca['SQZ-FC_LSC_DOF2_RSET'] = 2
                    self.timer['done'] = 0.1
                    shared.fc.OFS_IDX = not shared.fc.OFS_IDX
                    self.counter = 2
                    if self.even:
                        self.sweeprange *= 1
                        self.sweeptime *= 1
                    self.even = not self.even

        elif self.counter == 5:
            if shared.fc.RLF_locked():                
                ezca['SQZ-FC_GRD_RES_LO_OFFSET%d'%(shared.fc.OFS_IDX+1)] = ezca['SQZ-FC_LSC_FREQ_INMON'] + ezca['SQZ-FC_LSC_FREQ_OFFSET']
                ezca['SQZ-FC_LSC_FREQ_OFFSET'] = ezca['SQZ-FC_GRD_RES_LO_OFFSET%d'%(shared.fc.OFS_IDX+1)]
                self.timer['done'] = self.sweeptime
                self.counter += 1
            else:
                ezca['SQZ-FC_LSC_DOF2_GAIN'] = 0
                ezca.switch('SQZ-FC_LSC_DOF2','FM2','OFF')
                time.sleep(3)
                self.timer['done'] = 0.1
                shared.fc.OFS_IDX = not shared.fc.OFS_IDX
                self.counter = 2
                if self.even:
                    self.sweeprange *= 1
                    self.sweeptime *= 1
                self.even = not self.even
                
        else:
            return True

#------------------------------------------------------------------------------- 
class IRFREQ_RLF_LOCKED(GuardState):
    index = 101
    request = True
    
    @ttfss_checker
    @fault_checker
    @RLF_lock_checker
    def run(self):
        return True

#-------------------------------------------------------------------------------
class WFS_ENGAGING(GuardState):
    index = 300
    request = False

    def inmatrix(self):
        ezca['SQZ-FC_ASC_INMATRIX_P_SETTING_1_2'] = 1.135859e+03
        ezca['SQZ-FC_ASC_INMATRIX_Y_SETTING_1_2'] = 1.502048e+03
        ezca['SQZ-FC_ASC_INMATRIX_P_SETTING_1_4'] = -4.646675e+03
        ezca['SQZ-FC_ASC_INMATRIX_Y_SETTING_1_4'] = 5.401867e+03
        ezca['SQZ-FC_ASC_INMATRIX_P_SETTING_2_2'] = 1.883769e+01
        ezca['SQZ-FC_ASC_INMATRIX_Y_SETTING_2_2'] = -2.100436e+01
        ezca['SQZ-FC_ASC_INMATRIX_P_SETTING_2_4'] = -8.070106e+00
        ezca['SQZ-FC_ASC_INMATRIX_Y_SETTING_2_4'] = -6.967842e+00

    @ttfss_checker    
    @fault_checker
    @RLF_lock_checker
    def main(self):
        self.counter = 0
        self.timer['wait'] = 0

        self.fc_asc_gain = 0.5

    @ttfss_checker            
    @fault_checker        
    @RLF_lock_checker
    def run(self):
        if not self.timer['wait']:
            return

        if self.counter == 0:
            for sus in ['FC1','FC2']:
                for dof in ['P','Y']:
                    ezca.switch('SUS-%s_M1_LOCK_%s'%(sus,dof),'INPUT','ON')
            self.counter += 1

        elif self.counter == 1:            
            if ezca['SQZ-FC_ASC_GAIN'] < self.fc_asc_gain:
                ezca['SQZ-FC_ASC_GAIN'] += 0.1
                self.timer['wait'] = 1
                
            else:                
                self.counter += 1
                ezca['SQZ-FC_ASC_GAIN'] = self.fc_asc_gain
                self.timer['wait'] = 1
            
        elif self.counter == 2:            
            return True

#-------------------------------------------------------------------------------
class UPDATE_RES_OFFSET(GuardState):
    index = 350
    request = False

    @ttfss_checker        
    @fault_checker
    @RLF_lock_checker
    def main(self):
        ezca['SQZ-FC_SERVO_SLOW_TRAMP'] = 0
        self.SLOW_ofs_servo = Servo(ezca, 'SQZ-FC_SERVO_SLOW_OFFSET', readback='SQZ-FC_SERVO_SLOW_OUTPUT',
                                    gain=-100000, ugf=0.1)
        self.counter = 0
        self.timer['done'] = 0


    @ttfss_checker        
    @fault_checker
    @RLF_lock_checker

    def run(self):
        if not self.timer['done']:
            return
        
        if self.counter == 0:
            if abs(ezca['SQZ-FC_SERVO_SLOW_OUTPUT']) > 1e-4:
                self.SLOW_ofs_servo.step()
            else:
                self.counter += 1

        elif self.counter == 1:
            ezca['SQZ-FC_SERVO_SLOW_TRAMP'] = 1            
            ezca['SQZ-FC_GRD_RES_OFFSET%d'%(shared.fc.OFS_IDX+1)] = ezca['SQZ-FC_SERVO_SLOW_OFFSET']
            self.timer['done'] = 1
            self.counter += 1

        elif self.counter == 2:
            return True

#-------------------------------------------------------------------------------
class CLOSE_FCGS(GuardState):
    index = 360
    request = False

    @ttfss_checker        
    @fault_checker
    @RLF_lock_checker
    def main(self):

        #return True
        self.counter = 0
        self.timer['done'] = 0

    @ttfss_checker        
    @fault_checker
    @RLF_lock_checker
    def run(self):
        if not self.timer['done']:
            return
        
        if self.counter == 0:
            # open FCGS loop
            ezca['SQZ-FC_SERVO_COMBOOST'] = 0
            self.timer['done'] = 1
            self.counter += 1
            
        elif self.counter == 1:
            ezca['SQZ-FC_SERVO_SLOWBOOST'] = 0
            self.timer['done'] = 1
            self.counter += 1
            
        elif self.counter == 2:            
            ezca['SQZ-FC_SERVO_IN1EN'] = 0
            self.timer['done'] = 1
            self.counter += 1

        elif self.counter == 3:
            # close flipper
            ezca['SQZ-FC_FIBR_FLIPPER_CONTROL'] = 'Off'
            self.counter += 1
            
        elif self.counter == 4:
            return True

#-------------------------------------------------------------------------------
#WJ created these:
class SINGLE_WFS_ENGAGING(GuardState):
    index = 301
    request = False

    def inmatrix(self):
        ezca['SQZ-FC_ASC_INMATRIX_P_SETTING_1_2'] = 1.135859e+03
        ezca['SQZ-FC_ASC_INMATRIX_Y_SETTING_1_2'] = 1.502048e+03
        ezca['SQZ-FC_ASC_INMATRIX_P_SETTING_1_4'] = -4.646675e+03
        ezca['SQZ-FC_ASC_INMATRIX_Y_SETTING_1_4'] = 5.401867e+03
        ezca['SQZ-FC_ASC_INMATRIX_P_SETTING_2_2'] = 1.883769e+01
        ezca['SQZ-FC_ASC_INMATRIX_Y_SETTING_2_2'] = -2.100436e+01
        ezca['SQZ-FC_ASC_INMATRIX_P_SETTING_2_4'] = -8.070106e+00
        ezca['SQZ-FC_ASC_INMATRIX_Y_SETTING_2_4'] = -6.967842e+00    

    @ttfss_checker    
    @fault_checker
    @RLF_lock_checker
    def main(self):
        self.counter = 0
        self.timer['wait'] = 0



    @ttfss_checker            
    @fault_checker        
    @RLF_lock_checker
    def run(self):
        if not self.timer['wait']:
            return

        if self.counter == 0:
            for sus in ['FC1','FC2']:
                for dof in ['P','Y']:
                    ezca.switch('SUS-%s_M1_LOCK_%s'%(sus,dof),'INPUT','ON')
            self.counter += 1

        elif self.counter == 1:            
            if ezca['SQZ-FC_ASC_GAIN'] < 1:
                ezca['SQZ-FC_ASC_GAIN'] += 0.1
                self.timer['wait'] = 1
                
            else:                
                self.counter += 1
                ezca['SQZ-FC_ASC_GAIN'] = 1
                self.timer['wait'] = 1
            
        elif self.counter == 2:            
            return True

#-------------------------------------------------------------------------------
class BOOST_FCL(GuardState):
    index = 380
    @ttfss_checker        
    @fault_checker
    @RLF_lock_checker
    def main(self):
        ezca['SQZ-FC_LSC_DOF1_TRAMP'] = 1
        self.counter = 0
        self.timer['done'] = 0
        

    @ttfss_checker        
    @fault_checker
    @RLF_lock_checker
    def run(self):
        if not self.timer['done']:
            return
        
        if self.counter == 0:
            ezca['SQZ-FC_LSC_DOF1_GAIN'] = 2
            self.timer['done'] = 2
            self.counter += 1

        elif self.counter == 1:
            ezca.switch('SQZ-FC_LSC_DOF1','FM5','ON') 
            self.timer['done'] = 2
            self.counter += 1

        elif self.counter == 2:
            return True

#-------------------------------------------------------------------------------
class SINGLE_CLFRLF_LOCKED(GuardState):
    index = 351
    request = True

    @ttfss_checker        
    @fault_checker
    @RLF_lock_checker
    def main(self):
        ezca['SQZ-FC_SERVO_SLOW_TRAMP'] = 0
        self.SLOW_ofs_servo = Servo(ezca, 'SQZ-FC_SERVO_SLOW_OFFSET', readback='SQZ-FC_SERVO_SLOW_OUTPUT',
                                    gain=100000, ugf=0.1)
        self.counter = 0
        self.timer['done'] = 0


    @ttfss_checker        
    @fault_checker
    @RLF_lock_checker
    def run(self):
        if not self.timer['done']:
            return
        
        if self.counter == 0:
            if abs(ezca['SQZ-FC_SERVO_SLOW_OUTPUT']) > 1e-4:
                self.SLOW_ofs_servo.step()
            else:
                self.counter += 1

        elif self.counter == 1:
            ezca['SQZ-FC_SERVO_SLOW_TRAMP'] = 1            
            ezca['SQZ-FC_GRD_RES_OFFSET%d'%(shared.fc.OFS_IDX+1)] = ezca['SQZ-FC_SERVO_SLOW_OFFSET']
            self.timer['done'] = 1
            self.counter += 1

        elif self.counter == 2:
            node_LO.set_request('FINALIZED')
            self.counter += 1

        elif self.counter == 3:
            notify('waiting LO guardian to finalize')
            if node_LO.completed:
                self.counter += 1
        
        elif self.counter == 4:
            ezca.switch('SQZ-FC_LSC_DOF1','FM5','ON') # boost DOF1 filter bank
            time.sleep(1.0)
            return True

#-------------------------------------------------------------------------------
class SINGLE_CLOSE_FCGS(GuardState):
    index = 361
    request = False

    @ttfss_checker        
    @fault_checker
    @RLF_lock_checker
    def main(self):
        self.counter = 0
        self.timer['done'] = 0


    @ttfss_checker        
    @fault_checker
    @RLF_lock_checker
    def run(self):
        if not self.timer['done']:
            return
        
        if self.counter == 0:
            # open FCGS loop
            ezca['SQZ-FC_SERVO_COMBOOST'] = 0
            self.timer['done'] = 1
            self.counter += 1
            
        elif self.counter == 1:
            ezca['SQZ-FC_SERVO_SLOWBOOST'] = 0
            self.timer['done'] = 1
            self.counter += 1
            
        elif self.counter == 2:            
            ezca['SQZ-FC_SERVO_IN1EN'] = 0
            self.timer['done'] = 1
            self.counter += 1

        elif self.counter == 3:
            # close flipper
            ezca['SQZ-FC_FIBR_FLIPPER_CONTROL'] = 'Off'
            self.counter += 1
            
        elif self.counter == 4:
            return True

#-------------------------------------------------------------------------------
class WFS_ON(GuardState):
    index = 400
    request = True

    @ttfss_checker        
    @fault_checker
    @RLF_lock_checker
    def main(self):
        self.counter = 0
        self.timer['done'] = 0


    @ttfss_checker        
    @fault_checker
    @RLF_lock_checker
    def run(self):
        if not self.timer['done']:
            return
        
        if self.counter == 0:
            return True

#-------------------------------------------------------------------------------
class ENGAGING_BEAM_CENTERING(GuardState):
    index = 500
    request = False

    gainlist = {'PIT1':0,'PIT2':0,'PIT3':100,'PIT4':140,'YAW1':0,'YAW2':0,'YAW3':100,'YAW4':150}
    freqlist = {'PIT1':8,'PIT2':9,'PIT3':10,'PIT4':12,'YAW1':6,'YAW2':7,'YAW3':6,'YAW4':8}

    @ttfss_checker
    @fault_checker        
    @RLF_lock_checker        
    def main(self):
        self.timer['waiting'] = 0
        self.counter = 0

        for sus in ['FC1','FC2']:
            for stage in ['M1','M2']:
                for dof in ['P','Y']:
                    ezca.switch('SUS-%s_%s_DITHER_%s'%(sus,stage,dof),'OUTPUT','OFF')

        for sus in ['FC1','FC2']:
            for stage in ['M3']:
                for dof in ['P','Y']:
                    ezca.switch('SUS-%s_%s_DITHER_%s'%(sus,stage,dof),'OUTPUT','ON')
                    
        for sus in ['ZM1','ZM3']:
            for stage in ['M2']:
                for dof in ['P','Y']:
                    ezca.switch('SUS-%s_%s_DITHER_%s'%(sus,stage,dof),'OUTPUT','OFF')                        

        for sus in ['ZM1','ZM3']:
            for stage in ['M1']:
                for dof in ['P','Y']:
                    ezca.switch('SUS-%s_%s_DITHER_%s'%(sus,stage,dof),'OUTPUT','ON')

        # engage dither            
        for dof in self.gainlist:
            ezca['SQZ-FC_ASC_ADS_%s_OSC_TRAMP'%dof] = 3
            ezca['SQZ-FC_ASC_ADS_%s_OSC_FREQ'%dof] = self.freqlist[dof]
            ezca['SQZ-FC_ASC_ADS_%s_OSC_CLKGAIN'%dof] = self.gainlist[dof]

            self.timer['waiting'] = 10
        
    @ttfss_checker
    @fault_checker        
    @RLF_lock_checker        
    def run(self):
        if not self.timer['waiting']:
            return

        if self.counter == 0:
            for dof1 in ['POS','ANG']:
                for dof2 in ['P','Y']:
                    ezca.switch('SQZ-FC_ASC_INJ_%s_%s'%(dof1,dof2),'INPUT','ON')

            self.counter += 1
            self.timer['waiting'] = 3
                
        
        elif self.counter == 1:
            return True

#-------------------------------------------------------------------------------
class WAITING_ASC(GuardState):
    index = 600
    request = False

    DOFLIST = ['CAV_POS','CAV_ANG','INJ_POS','INJ_ANG']

    def popappend(self,nplist,value):
        if nplist[-1] == value:
            return nplist
        else:
            return np.append(nplist[1:],value)

    @ttfss_checker        
    @fault_checker
    @RLF_lock_checker
    def main(self):        
        self.counter = 0

        self.timer['done'] = 0
        self.checktime = 5


        self.dataT = 45 # sec                
        self.timer['data_acq'] = self.dataT+1
        
        self.dataset = {}
        for dof1 in self.DOFLIST:
            for dof2 in ['P','Y']:            
                self.dataset[dof1+dof2] = np.zeros(self.dataT*16)

    @ttfss_checker        
    @fault_checker
    @RLF_lock_checker
    def run(self):
        if not self.timer['done']:
            return

        for dof1 in self.DOFLIST:
            for dof2 in ['P','Y']:
                filt = ezca.get_LIGOFilter('SQZ-FC_ASC_%s_%s'%(dof1,dof2))
                self.dataset[dof1+dof2] = self.popappend(self.dataset[dof1+dof2],ezca['SQZ-FC_ASC_%s_%s_INMON'%(dof1,dof2)]+ezca['SQZ-FC_ASC_%s_%s_OFFSET'%(dof1,dof2)]*filt.is_on('OFFSET'))

        if not self.timer['data_acq']:
            self.timer['ASC_zero'] = self.checktime
            return

        ASC_done = True
        msg = 'waiting for '
        for dof1 in self.DOFLIST:
            for dof2 in ['P','Y']:
                dataset = self.dataset[dof1+dof2]
                if abs(np.mean(dataset)) > np.std(dataset)/1.5:
                    ASC_done = False
                    msg += dof1 + ' ' + dof2 + ', '


        if ASC_done:
            return self.timer['ASC_zero']
        else:
            notify(msg)
            self.timer['ASC_zero'] = self.checktime
            return

#-------------------------------------------------------------------------------
class DISABLING_BEAM_CENTERING(GuardState):
    index = 900
    request = False

    @ttfss_checker
    @fault_checker        
    @RLF_lock_checker        
    def main(self):
        self.timer['waiting'] = 0
        self.counter = 0
        
    @ttfss_checker
    @fault_checker        
    @RLF_lock_checker        
    def run(self):
        if not self.timer['waiting']:
            return

        if self.counter == 0:
            for dof1 in ['POS','ANG']:
                for dof2 in ['P','Y']:
                    ezca.switch('SQZ-FC_ASC_INJ_%s_%s'%(dof1,dof2),'INPUT','OFF')

            self.counter += 1
            self.timer['waiting'] = 1
                
        
        elif self.counter == 1:
            for dof in ['PIT','YAW']:
                for ii in ['1','2','3','4']:
                    ezca['SQZ-FC_ASC_ADS_%s_OSC_CLKGAIN'%(dof+ii)] = 0
            self.counter += 1

        elif self.counter == 2:
            return True

#-------------------------------------------------------------------------------
class FREEZE_BEAM_CENTERING(GuardState):
    index = 813
    request = False

    @ttfss_checker
    @fault_checker        
    @RLF_lock_checker        
    def main(self):
        self.timer['waiting'] = 0
        self.counter = 0
        
    @ttfss_checker
    @fault_checker        
    @RLF_lock_checker        
    def run(self):
        if not self.timer['waiting']:
            return

        if self.counter == 0:
            for dof1 in ['POS','ANG']:
                for dof2 in ['P','Y']:
                    ezca.switch('SQZ-FC_ASC_INJ_%s_%s'%(dof1,dof2),'HOLD','ON')
                    ezca.switch('SQZ-FC_ASC_CAV_%s_%s'%(dof1,dof2),'HOLD','ON')
                    
                    ezca.switch('SQZ-FC_ASC_INJ_%s_%s'%(dof1,dof2),'INPUT','OFF')
                    ezca.switch('SQZ-FC_ASC_CAV_%s_%s'%(dof1,dof2),'INPUT','OFF')

            self.counter += 1
            self.timer['waiting'] = 1

        elif self.counter == 1:
            return True
#-------------------------------------------------------------------------------

class UNFREEZE_BEAM_CENTERING(GuardState):
    index = 814
    request = False

    @ttfss_checker
    @fault_checker        
    @RLF_lock_checker        
    def main(self):
        self.timer['waiting'] = 0
        self.counter = 0
        
    @ttfss_checker
    @fault_checker        
    @RLF_lock_checker        
    def run(self):
        if not self.timer['waiting']:
            return

        if self.counter == 0:
            for dof1 in ['POS','ANG']:
                for dof2 in ['P','Y']:
                    ezca.switch('SQZ-FC_ASC_INJ_%s_%s'%(dof1,dof2),'HOLD','OFF')
                    ezca.switch('SQZ-FC_ASC_CAV_%s_%s'%(dof1,dof2),'HOLD','OFF')
                    
                    ezca.switch('SQZ-FC_ASC_INJ_%s_%s'%(dof1,dof2),'INPUT','ON')
                    ezca.switch('SQZ-FC_ASC_CAV_%s_%s'%(dof1,dof2),'INPUT','ON')

            self.counter += 1
            self.timer['waiting'] = 1

        elif self.counter == 1:
            return True
#-------------------------------------------------------------------------------
class FINALIZING_LO(GuardState):
    index = 700
    request = False

    @ttfss_checker        
    @fault_checker
    @RLF_lock_checker
    def main(self):
        self.counter = 0
        self.timer['done'] = 0


    @ttfss_checker        
    @fault_checker
    @RLF_lock_checker
    def run(self):
        if not self.timer['done']:
            return
        
        if self.counter == 0:       
            node_LO.set_request('FINALIZED')
            self.counter += 1

        elif self.counter == 1:
            notify('waiting LO guardian to finalize')
            if node_LO.completed:
                self.counter += 1

        elif self.counter == 2:
            return True

#-------------------------------------------------------------------------------
class ENGAGING_AS42(GuardState):
    index = 800
    request = False

    
    def inmatrix(self):
        ZM4toAS_A = {'PIT':1,'YAW':-0.00128/60,}
        ZM4toAS_B = {'PIT':1,'YAW':-0.0196/60,}

        ZM6toAS_A = {'PIT':1,'YAW':0.0303/60,}
        ZM6toAS_B = {'PIT':1,'YAW':0.0148/60,}
        
        
        

    @ttfss_checker    
    @fault_checker
    @RLF_lock_checker
    def main(self):
        self.counter = 0
        self.timer['wait'] = 0

    @ttfss_checker            
    @fault_checker        
    @RLF_lock_checker
    def run(self):
        if not self.timer['wait']:
            return

        if self.counter == 0:
            for sus in ['FC1','FC2']:
                for dof in ['P','Y']:
                    ezca.switch('SUS-%s_M1_LOCK_%s'%(sus,dof),'INPUT','ON')
            self.counter += 1

        elif self.counter == 1:            
            if ezca['SQZ-FC_ASC_GAIN'] < 1:
                ezca['SQZ-FC_ASC_GAIN'] += 0.1
                self.timer['wait'] = 1
                
            else:                
                self.counter += 1
                ezca['SQZ-FC_ASC_GAIN'] = 1
                self.timer['wait'] = 1



            
        elif self.counter == 2:            
            return True

#-------------------------------------------------------------------------------
class LOCKED(GuardState):
    index = 1000
    request = True

    @ttfss_checker        
    @fault_checker
    @RLF_lock_checker
    def main(self):
        self.counter = 0
        self.timer['done'] = 0


    @ttfss_checker        
    @fault_checker
    @RLF_lock_checker
    def run(self):
        if not self.timer['done']:
            return
        
        if self.counter == 0:
            return True

edges = [
    ('INIT','DOWN'),
    ('DOWN','IDLE'),
    ('FAULT','DOWN'),
    ('IDLE','MANAGE_DEPENDENCIES',1),
    ('MANAGE_DEPENDENCIES', 'NORMALIZE_WFS'), #new for IR hand-off
    ('NORMALIZE_WFS','GR_LOCKING'), #new for IR hand-off
    #('MANAGE_DEPENDENCIES','GR_LOCKING'), #O4a
    ('GR_LOCKING','GR_MASS_FB'),
    ('GR_MASS_FB','GR_LOCKED'),

    #('PREP_FOR_IR','CLFRLF_MASS_FB713'),
    #('GR_LOCKED','CLFRLF_MASS_FB'),
    ('GR_LOCKED','PREP_FOR_IR',2),
    ('GR_LOCKED','CLFRLF_MASS_FB_1'),
    ('CLFRLF_MASS_FB_1','CLFRLF_MASS_FB_2'),
    ('CLFRLF_MASS_FB_2','RLF_LOCKED'),
    #('CLFRLF_MASS_FB713','RLF_LOCKED'),
    ('PREP_FOR_IR','PRE_SCAN'),
    #('PREP_FOR_IR', 'RLF_LOCKED'),
    ('PRE_SCAN','LOCK_IR'),
    ('LOCK_IR', 'RLF_LOCKED'),
    #('PRE_SCAN', 'RLF_LOCKED'),
    #('PREP_FOR_IR', 'RLF_LOCKED'),
    ('RLF_LOCKED','WFS_ENGAGING'),
    ('WFS_ENGAGING','UPDATE_RES_OFFSET'),
    #('UPDATE_RES_OFFSET','CLOSE_FCGS'),
    #('CLOSE_FCGS','WFS_ON'),
    ('UPDATE_RES_OFFSET','WFS_ON'),
    ('WFS_ON','ENGAGING_BEAM_CENTERING'),
    ('ENGAGING_BEAM_CENTERING','FINALIZING_LO'),
    ('FINALIZING_LO','LOCKED'),
    
    ('LOCKED', 'FREEZE_BEAM_CENTERING'),
    ('FREEZE_BEAM_CENTERING', 'UNFREEZE_BEAM_CENTERING'),
    ('UNFREEZE_BEAM_CENTERING','FREEZE_BEAM_CENTERING'),
    ('UNFREEZE_BEAM_CENTERING','LOCKED'),

    ('GR_LOCKED','SINGLE_CLFRLF_MASS_FB'),
    ('SINGLE_CLFRLF_MASS_FB','SINGLE_RLF_LOCKED'),
    ('SINGLE_RLF_LOCKED','SINGLE_WFS_ENGAGING'),
    ('SINGLE_WFS_ENGAGING','SINGLE_CLFRLF_LOCKED'),
    

    #('MANAGE_DEPENDENCIES', 'NORMALIZE_WFS'), #new for IR hand-off
    ('LOCKED','DISABLING_BEAM_CENTERING'),
    ('DISABLING_BEAM_CENTERING','WFS_ON'),


    ('IDLE','IRFREQ_LOCKING',2),
    ('IRFREQ_LOCKING','IRFREQ_MASS_FB'),
    ('IRFREQ_MASS_FB','IRFREQ_LOCKED'),
    ('IRFREQ_LOCKED','IRFREQ_RLF_LOCKING'),
    ('IRFREQ_RLF_LOCKING','IRFREQ_RLF_LOCKED'),  
    ('IDLE','MISALIGN_FC2'),  
    #('IRFREQ_RLF_LOCKED','ASC_LOCKING'),
]

